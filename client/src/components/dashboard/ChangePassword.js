import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import AuthFieldGroup from "../common/AuthFieldGroup";
import { resetPassword } from "../../actions/authActions";
import propTypes from "prop-types";
import styled from "styled-components";

const Input = styled.div`
  max-width: 170px;
  width: 100%;
`;

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      passwordChange: "",
      newPasswordChange: "",
      confirmNewPasswordChange: "",
      errors: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    // console.log(this.props);
    // console.log(this.state);

    const resetData = {
      // id: this.auth.user.id,
      password: this.state.passwordChange,
      newPassword: this.state.newPasswordChange,
      confirmNewPassword: this.state.confirmNewPasswordChange
    };

    this.props.resetPassword(resetData);
  };

  render() {
    const { errors } = this.props;
    console.log(errors);
    return (
      <Fragment>
        <h4 className="mb-5 center-mobile">Changer le mot de passe </h4>
        <form onSubmit={this.onSubmit}>
          <Input>
            <AuthFieldGroup
              placeholder="Mot de passe actuel"
              name="passwordChange"
              icon=""
              type="password"
              value={this.state.passwordChange}
              onChange={this.onChange}
              error={errors.password}
            />

            <AuthFieldGroup
              placeholder="Nouveau mot de passe"
              name="newPasswordChange"
              icon=""
              type="password"
              value={this.state.newPasswordChange}
              onChange={this.onChange}
              error={errors.newpassword}
            />

            <AuthFieldGroup
              placeholder="Confirmer nouveau mot de passe"
              name="confirmNewPasswordChange"
              icon=""
              type="password"
              value={this.state.confirmNewPasswordChange}
              onChange={this.onChange}
              error={errors.confirmNewPassword}
            />
          </Input>
          <input type="submit" value="Confirmer" className="btn btn-info" />
        </form>
      </Fragment>
    );
  }
}

ChangePassword.propTypes = {
  resetPassword: propTypes.func.isRequired,
  profile: propTypes.object.isRequired,
  auth: propTypes.object.isRequired,
  errors: propTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { resetPassword }
)(ChangePassword);
