import React from 'react';
import { Link } from 'react-router-dom';


const ProfileActions = () => {
  return (
    <div className="btn-group mb-4" role="group">
      <Link to="/edit-profile" className="btn btn-light">
        <i className="fas fa-user-circle text-info mr-1" /> 
        Modifier Profile
      </Link>
      <Link to="/add-annonce" className="btn btn-light">
        <i className="fab fa-black-tie text-info mr-1" />
        Ajouter Annonce
      </Link>
      <Link to="/add-agence" className="btn btn-light">
        <i className="fas fa-desktop text-info mr-1" />
        ajouter Agence
      </Link>
      <Link to="/Change-password" className="btn btn-light">
      <i className="fas fa-key text-info mr-1 "></i>
        Changer mot de passe 
      </Link>
    </div>
    
  );
};

export default ProfileActions;
// export function modal(){
  
//   return (
//     <div className="modal">
//       const [modalIsOpen, setModalIsOpen] = useState(false)
//       <Button onClick={()=>setModalIsOpen(true)}>Open Modal</Button>
//       <Modal 
//       isOpen={modalIsOpen} 
//       shouldCloseOnOverlayClick={false}
//       onRequestClose={() => setModalIsOpen(false)}>
//         <h2>Modal title</h2>
//         <p>Modal body</p>*
//         <div>
//           <Button onClick={()=> setModalIsOpen(false)}>close</Button>
//         </div>
//       </Modal>
//     </div>
//   )
// }