import React, { Component } from 'react';


class ProfileCreds extends Component {
  render() {
    const { annonce, agence } = this.props;

    const annItems = annonce.map(ann => (
      <li key={ann._id} className="list-group-item">
        <h4>{ann.title}</h4>
        
        
        <p>
          {ann.adressebien === '' ? null : (
            <span>
              <strong>Adresse: </strong> {ann.adressebien}
            </span>
          )}
        </p>
        <p>
          {ann.status === '' ? null : (
            <span>
              <strong>Status: </strong> {ann.status}
            </span>
          )}
        </p>
        <p>
          {ann.prix === '' ? null : (
            <span>
              <strong>Prix: </strong> {ann.prix}
            </span>
          )}
        </p>
        

      </li>
    ));

    const ageItems = agence.map(age => (
      <li key={age._id} className="list-group-item">
        <h4>{age.nom}</h4>
        
        <p>
          <strong>Adresse:</strong> {age.adresse}
        </p>
        <p>
          <strong>Région:</strong> {age.region}
        </p>
        
      </li>
    ));
    return (
      <div className="row">
        <div className="col-md-6">
          <h3 className="text-center text-info">Annonce</h3>
          {annItems.length > 0 ? (
            <ul className="list-group">{annItems}</ul>
          ) : (
            <p className="text-center">Aucune annonce répertoriée</p>
          )}
        </div>

        <div className="col-md-6">
          <h3 className="text-center text-info">Agence</h3>
          {ageItems.length > 0 ? (
            <ul className="list-group">{ageItems}</ul>
          ) : (
            <p className="text-center">Aucune agence répertoriée</p>
          )}
        </div>
      </div>
    );
  }
}

export default ProfileCreds;
