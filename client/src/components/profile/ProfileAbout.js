import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const ProfileAbout =({ 
    profile: {
        bio,
        user: {name}
}
})=> {
    return (
        <div className="row">
            <div className="col-md-12">
            <div className="card card-body bg-light mb-3">
            {bio && (
                <Fragment>
                        <h3 className="text-center text-info">{name.trim().split(' ')[0]}'s Bio</h3>
                        <p className="lead">
                            {bio}
                        </p>
                        
                </Fragment>
            )}
          
          
          
          </div>
          </div>
        </div>
    )
};

ProfileAbout.propTypes = {
    profile: PropTypes.object.isRequired
};

export default ProfileAbout;
