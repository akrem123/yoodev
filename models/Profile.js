const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const ProfileSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  handle: {
    type: String,
    required: true,
    max: 40
  },
  poste: {
    type: String,
    required:true
  },
  tel: {
    type: String
  },
  emailadr: {
    type: String
  },
  
  bio: {
    type: String
  },
  
  annonce: [
    {
      title: {
        type: String,
        required: true
      },
      prix: {
        type: String,
        required: true
      },
      adressebien: {
        type: String
        
      },
      typeannonce: {
        type: String
      },
      status: {
        type: String,
        required: true
      },
      surface: {
        type: String
      },
      description: {
        type: String
      },
      productPic: [
        {
            img: String
        }
    ]
    }
  ],
  agence: [
    {
      nom: {
        type: String,
        required: true
      },
      adresse: {
        type: String,
        required: true
      },
      region: {
        type: String,
        required: true
      }
      
    }
  ],
  social: {
    
   
    facebook: {
      type: String
    },
    linkedin: {
      type: String
    },
    instagram: {
      type: String
    }
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Profile = mongoose.model('profile', ProfileSchema);
