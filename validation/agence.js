const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateAgenceInput(data) {
  let errors = {};

  data.nom = !isEmpty(data.nom) ? data.nom : '';
  data.adresse= !isEmpty(data.adresse) ? data.adresse : '';
  data.region = !isEmpty(data.region) ? data.region : '';
  

  if (Validator.isEmpty(data.nom)) {
    errors.nom = 'Le champ Nom est obligatoire';
  }

  if (Validator.isEmpty(data.adresse)) {
    errors.adresse = 'Le champ adresse est obligatoire';
  }

  if (Validator.isEmpty(data.region)) {
    errors.region = 'Le champ Région Agence est obligatoire';
  }

  

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
